import React from "react";
import { BeatLoader } from "react-spinners";
import styled from "styled-components";

const Spinner = () => {
    return (
        <div>
            <h className="text-4xl">잠시만 기다려주세요.</h>
            <SpinnerWrapper>
                <BeatLoader margin={5} size={15} />
            </SpinnerWrapper>
        </div>
    );
};

export default Spinner;

const SpinnerWrapper = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  background: #ffffffb7;
  z-index: 999;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
