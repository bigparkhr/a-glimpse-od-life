'use client'

import {useState} from "react";
import axios from "axios";
import Spinner from "@/components/Spinner";

export default function Search() {
    const [question, setQuestion] = useState('')
    const [response, setResponse] = useState('')
    const [loading, setLoading] = useState(false)

    const handleQuestion = e => {
        setQuestion(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()

        setLoading(true) // API 호출 전에 로딩 상태를 true로 설정

        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyCf4QU49J2sYn-VniprgL8ZhUyW47--GFo'

        const data = {"contents": [{"parts": [{"text": question}]}]}
        axios.post(apiUrl, data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text)
                setLoading(false) // API 호출이 성공하면 로딩 상태를 false로 설정
            })
            .catch(err => {
                console.log(err)
                setLoading(false) // API 호출이 실패하면 로딩 상태를 false로 설정
            })
    }

    return (
        <main className="flex min-h-screen flex-col items-center p-24 bg-orange-50">
            {loading ? (
                <div className="items-center"><Spinner /></div>
            ) : (
                <>
                    <div>
                        <h2 className={`mb-3 text-6xl font-bold`}>Search for your desired song</h2>
                        <p className="ml-64 justify-center text-xl">Search and explore the details</p>
                        <div
                            className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
                            <a
                                className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
                                href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                            </a>
                        </div>
                    </div>
                    <div>
                        <form onSubmit={handleSubmit}>
                            <div className="my-[80px] justify-center items-center">
                                <div className="relative">
                                    <input
                                        type="text"
                                        value={question}
                                        onChange={handleQuestion}
                                        className="w-[900px] h-20 pl-10 pr-4 border rounded-lg"
                                        placeholder="Search for your desired song"
                                    />
                                    {/*<button type="submit">확인</button>*/}
                                    <div className="absolute inset-y-0 left-3 flex items-center pointer-events-none">
                                        <svg className="h-7 w-7 text-gray-400" fill="none" viewBox="0 0 24 24"
                                             stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                                  d="M10 17l5-5m0 0l-5-5m5 5H4"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </form>
                        {response && <p className="justify-center items-center ">{response}</p>}
                    </div>
                </>
            )}
        </main>
    );
}