'use client'

import { useState } from "react";
import axios from "axios";

export default function CallPage() {
    const [question, setQuestion] = useState('')
    const [response, setResponse] = useState('')

    const handleQuestion = e => {
        setQuestion(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()

        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=AIzaSyCf4QU49J2sYn-VniprgL8ZhUyW47--GFo'

        const data = { "contents":[{"parts":[{"text": question}]}]}
        axios.post(apiUrl, data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text)
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <div>
            <foem onSubmit={handleSubmit}>
                <input type={'text'} value={question} onChange={handleQuestion} placeholder={'what kind of you?'} />
                <button type={'submit'}>Check</button>
            </foem>
            <p>All you need id love</p>
            {response && <p>{response}</p>}
        </div>
    )
}