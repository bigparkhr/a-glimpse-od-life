'use client'

import Image from "next/image";
import { useRouter } from "next/navigation";

export default function Home() {
    const router = useRouter()

    return (
      <main className="flex min-h-screen flex-col items-center justify-between p-24 bg-orange-50">
        <div className="">
          <h2 className={`mb-3 text-6xl font-bold`}>Hello, welcome!</h2>
          <p className="ml-8 justify-cente text-xl">What kind of music do you want to know?</p>
          <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
            <a
                className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
                href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
                target="_blank"
                rel="noopener noreferrer"
            >
            </a>
          </div>
        </div>

        <div className="">
          <Image
              className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
              src="/dasiy.jpeg"
              alt="beatuiful"
              width={500}
              height={300}
              priority
          />
        </div>

        <div className="justify-center items-center">
          <div onClick={() => router.push("/search")}
              href={'/search'}
              className="group rounded-lg transition-colors hover:bg-transparent hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
              target="_blank"
              rel="noopener noreferrer"
          >
            <h2 className={`mb-3 text-3xl font-semibold`}>
              Start{" "}
              <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
            </h2>
            <p className={`m-0 max-w-[30ch] text-ml opacity-50`}>
                Let's dive into the history of the music you're looking for..
            </p>
          </div>
        </div>
      </main>
  );
}
